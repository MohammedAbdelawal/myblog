from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField( User, on_delete=models.CASCADE)
    pic = models.ImageField(upload_to = 'profiles/' )
    about = models.TextField(max_length=2001)
    fb_link = models.CharField(max_length=500)


    def __str__(self):
        return self.user.username

class Category(models.Model):

    name = models.CharField(max_length=50)
    desc = models.TextField()
    pic = models.CharField( default='blank.jpg' , max_length=50)

    def __str__(self):
        return self.name


class Article(models.Model):
    pub_date = models.DateField(auto_now=True)
    auther = models.ForeignKey(User, on_delete= models.CASCADE) 
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    title = models.CharField(unique=True,max_length=120)
    pic = models.ImageField(upload_to = 'blogs/' )
    article = models.TextField()
    
    def __str__(self):
        return self.title


class Comment(models.Model):
    auther = models.ForeignKey(User,on_delete=models.CASCADE)
    Blog = models.ForeignKey(Article,on_delete=models.CASCADE)
    comment = models.TextField()


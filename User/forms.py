from django import forms
from django.contrib.auth.models import User
from .models import Profile


class UserForm(forms.ModelForm):
    username = forms.CharField(label='اسم المستخدم')
    password = forms.CharField(widget = forms.PasswordInput ,label= 'كلمة المرور')
    email = forms.EmailField(label='البريد الالكترونى')

    class Meta:
        model = User
        fields = ['username', 'email', 'password']

class ProfileForm(forms.ModelForm):

    pic = forms.ImageField(label='صورتك الشخصية')
    about = forms.CharField(widget = forms.widgets.Textarea(), label='وصف', required=False)
    fb_link = forms.URLField(label='رابط الفيسبوك')
    class Meta:
        model = Profile
        fields = ['pic', 'about', 'fb_link']



    
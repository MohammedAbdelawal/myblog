from django.contrib import admin
from django.urls import path
from User import views 

urlpatterns = [
    path('', views.CategoryListView.as_view(), name = 'main'),
    path('admin/', admin.site.urls),
    path('Home/', views.home, name='home'),
    path('accounts/register/',views.register, name='register' ),
    path('accounts/login/',views.loginPage, name='login' ),
    path('accounts/logout/',views.logoutPage, name='logout' ),
    ]
